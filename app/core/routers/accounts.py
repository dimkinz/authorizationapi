import hashlib

from datetime import timedelta
from pydantic import BaseModel

from fastapi import APIRouter, HTTPException
from fastapi import Depends
from fastapi_jwt_auth import AuthJWT

from core.models import accounts_schemas
from core.utils import get_terminal_by_name, create_user, get_terminal_by_pincode, get_hashed_pincode
from core.config import SECRET_KEY, PINCODE_LENGTH, ACCESS_TOKEN_TIME, REFRESH_TOKEN_TIME, ADMIN_KEY, ACCOUNT_SALT

EXPIRES_ACCESS_TOKEN = timedelta(minutes=ACCESS_TOKEN_TIME)
EXPIRES_REFRESH_TOKEN = timedelta(days=REFRESH_TOKEN_TIME)


router = APIRouter()


class Settings(BaseModel):
    authjwt_secret_key: str = SECRET_KEY
    authjwt_decode_algorithms: set = {'HS256', "HS384", "HS512"}


@AuthJWT.load_config
def get_config():
    return Settings()


@router.post('/api/v1/signup', response_description='Add Terminal')
async def create_terminal(
        terminal: accounts_schemas.AdminUser) -> dict:

    if terminal.admin_key != ADMIN_KEY:
        raise HTTPException(status_code=401, detail='Access is denied')

    if len(terminal.pincode) != PINCODE_LENGTH:
        raise HTTPException(status_code=400,
                            detail=f'PIN code must be equal to {PINCODE_LENGTH} numbers')

    hashed_pincode = get_hashed_pincode(terminal.pincode, ACCOUNT_SALT)

    db_pincode = await get_terminal_by_pincode(hashed_pincode)
    if db_pincode:
        raise HTTPException(status_code=400, detail='Pin-code is registered')

    terminal.pincode = hashed_pincode
    await create_user(terminal=terminal)
    return {'Detail': 'Terminal created'}


@router.post('/api/v1/login', response_model=accounts_schemas.Token, tags=['Authorization'])
async def login(terminal: accounts_schemas.LoginUser, Authorize: AuthJWT = Depends()):  # noqa B008
    if len(terminal.pincode) != PINCODE_LENGTH:
        raise HTTPException(status_code=400, detail=f'PIN code must be equal to {PINCODE_LENGTH} numbers')

    hashed_pincode = get_hashed_pincode(terminal.pincode, ACCOUNT_SALT)

    db_terminal = await get_terminal_by_pincode(hashed_pincode)

    if db_terminal:
        if not db_terminal.is_active:
            raise HTTPException(status_code=400, detail='Inactive terminal')

        pincode = {'pincode': terminal.pincode}

        access_token = Authorize.create_access_token(subject=db_terminal.terminal, algorithm="HS256",
                                                     user_claims=pincode, expires_time=EXPIRES_ACCESS_TOKEN)
        refresh_token = Authorize.create_refresh_token(subject=db_terminal.terminal, algorithm="HS256",
                                                       user_claims=pincode, expires_time=EXPIRES_REFRESH_TOKEN)

        return {"access_token": access_token, "refresh_token": refresh_token}

    raise HTTPException(status_code=400, detail='Pin-code not found')


@router.post('/api/v1/refresh', response_model=accounts_schemas.Token, tags=['Authorization'])
def refresh(Authorize: AuthJWT = Depends()): # noqa B008

    try:
        Authorize.jwt_refresh_token_required()
    except Exception as err:
        raise HTTPException(status_code=err.status_code, detail=err.message)

    current_user = Authorize.get_jwt_subject()
    event = Authorize.get_raw_jwt()['pincode']
    pincode = {'pincode': event}

    new_access_token = Authorize.create_access_token(subject=current_user, algorithm="HS256",
                                                     user_claims=pincode, expires_time=EXPIRES_ACCESS_TOKEN)
    new_refresh_token = Authorize.create_refresh_token(subject=current_user, algorithm="HS256",
                                                       user_claims=pincode, expires_time=EXPIRES_REFRESH_TOKEN)
    return {"access_token": new_access_token, "refresh_token": new_refresh_token}


@router.get('/api/v1/protected', tags=['Authorization'])
def protected(Authorize: AuthJWT = Depends()):  # noqa B008

    try:
        Authorize.jwt_required()
    except Exception as err:
        raise HTTPException(status_code=err.status_code, detail=err.message)

    terminal = Authorize.get_jwt_subject()
    return {"Terminal": terminal}
