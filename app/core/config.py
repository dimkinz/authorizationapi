import os

SECRET_KEY = os.environ.get('SECRET_KEY')
ADMIN_KEY = os.environ.get('ADMIN_KEY')
ACCOUNT_SALT = os.environ.get('ACCOUNT_SALT')
PINCODE_LENGTH = int(os.environ.get('PINCODE_LENGTH', 9))
ACCESS_TOKEN_TIME = int(os.environ.get('EXPIRES_ACCESS_TOKEN', 60))
REFRESH_TOKEN_TIME = int(os.environ.get('EXPIRES_REFRESH_TOKEN', 1))

# Postgres
DB_USER = os.environ.get('API_PG_USER', 'postgres')
DB_PASS = os.environ.get('API_PG_PASSWORD', 'postgres')
DB_HOST = os.environ.get('API_PG_HOST', 'localhost')
DB_PORT = os.environ.get('API_PG_PORT', 5432)
DB_NAME = os.environ.get('API_PG_NAME', 'postgres')
DB_SSL = os.environ.get('API_PG_SSL') in ('True', 'true')
