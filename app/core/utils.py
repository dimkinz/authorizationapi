import hashlib

from core.database import database
from core.models.accounts import accounts_table as accounts
from core.models import accounts_schemas


async def get_terminal_by_name(terminal: str):
    """Возвращаем информацию о пользователе"""
    query = accounts.select().where(accounts.c.terminal == terminal)
    return await database.fetch_one(query)


async def get_terminal_by_pincode(hashed_pincode: str):
    """Возвращаем информацию о пользователе"""
    query = accounts.select().where(accounts.c.pincode == hashed_pincode)
    return await database.fetch_one(query)


async def create_user(terminal: accounts_schemas.UserCreate):
    """Создаем нового пользователя в БД"""
    query = accounts.insert().values(
        terminal=terminal.terminal,
        pincode=terminal.pincode,
    )
    await database.execute(query)


def get_hashed_pincode(pincode: str, salt: str, algorithm: str = 'sha256', our_app_iters: int = 500000) -> str:
    return hashlib.pbkdf2_hmac(algorithm, pincode.encode(), salt.encode(), our_app_iters).hex()
