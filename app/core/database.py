import databases

from core.config import DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME, DB_SSL

# Postgres DB
SQLALCHEMY_DATABASE_URL = (f'postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}')
database = databases.Database(SQLALCHEMY_DATABASE_URL)
