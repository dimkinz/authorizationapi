import sqlalchemy

metadata = sqlalchemy.MetaData()

accounts_table = sqlalchemy.Table(
    'accounts',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('terminal', sqlalchemy.String(20), default='Mobile'),
    sqlalchemy.Column('pincode', sqlalchemy.String(255)),
    sqlalchemy.Column(
        'is_active',
        sqlalchemy.Boolean(),
        server_default=sqlalchemy.sql.expression.true(),
        nullable=False,
    ),
)
