from pydantic import BaseModel


class UserCreate(BaseModel):
    """Проверка Sing-up"""
    terminal: str = 'Mobile'
    pincode: str


class UserBase(BaseModel):
    """Ответ с деталями"""
    id: int  # noqa A003
    terminal: str


class Token(BaseModel):
    access_token: str
    refresh_token: str


class LoginUser(BaseModel):
    pincode: str


class AdminUser(UserCreate):
    """Проверка на суперадмина"""
    admin_key: str
