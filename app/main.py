import uvicorn

from fastapi import FastAPI

from core.database import database

from core.routers.accounts import router as Router


app = FastAPI(title='Authorization API')
app.include_router(Router, tags=['Authorization'])


@app.on_event('startup')
async def start_bd():
    await database.connect()


@app.get("/")
async def root():
    return {"Name service": "Authorization API"}


if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8001)
