# Authorization

## Предварительные настройки
Создать в корне файл .env с переменными

```bash
SECRET_KEY=<Секретный ключ>
ADMIN_KEY=<Ключ для созданя терминалов>
PINCODE_LENGTH=<Длина пин-кода>
EXPIRES_ACCESS_TOKEN=<Время жизни токена доступа>
EXPIRES_REFRESH_TOKEN=<Время жизни рефреш-токена>

# Postgres
DB_USER=postgres
DB_PASS=postgres
DB_HOST=postgres-db
DB_PORT=5432
DB_NAME=postgres
```

## Запуск Бэкенд

```bash
docker-compose up -d
```