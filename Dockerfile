FROM python:3.10-alpine3.16 as builder

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev g++

# install dependencies
COPY ./requirements.txt .
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt


#########
# FINAL #
#########

# pull official base image
FROM python:3.10-alpine3.16

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
RUN addgroup -S app && adduser -S app -G app

# create the appropriate directories
ENV APP_HOME=/home/app/app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# install dependencies
RUN apk update && apk add libpq
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN pip install --no-cache /wheels/*

# copy project
COPY ./app $APP_HOME
WORKDIR ${APP_HOME}

# lint
RUN pip install --upgrade pip
RUN pip install flake8==5.0.4 flake8-bandit\
    flake8-broken-line\
    flake8-bugbear\
    flake8-builtins\
    flake8-commas\
    flake8-comprehensions\
    flake8-debugger\
    flake8-eradicate\
    flake8-executable\
    flake8-fixme\
    flake8-pyi\
    flake8-pytest\
    flake8-pytest-style\
    flake8-mutable\
    flake8-string-format\
    flake8-todo\
    flake8-unused-arguments
RUN flake8 --ignore=E501,F401,E800,EXE002 --per-file-ignores=*/test*.py:S101 .

# chown all the files to the app user
RUN chown -R app:app $APP_HOME

# change to the app user
USER app
WORKDIR ${APP_HOME}
